def factorial(n):
	if n==1:
		return 1
	else:
		result=n*factorial(n-1) #n=5 * 24 = result = 120
					#n=4 * 6  = result = 24
					#n=3 * 2  = result = 6
					#n=2 * 1  = result = 2
					#n=1
		return result
print (factorial(6)) #n=6 * 120 = result = 720

'''O/p: 720 '''pyth
